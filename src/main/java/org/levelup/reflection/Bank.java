package org.levelup.reflection;

import java.util.UUID;

@SuppressWarnings("ALL")
public class Bank {

    private String license;
    public String name;

    public Bank(String name) {
        this(name, UUID.randomUUID().toString());
    }

    private Bank(String name, String license) {
        this.name = name;
        this.license = license;
    }

    private void regenerateLicense() {
        license = UUID.randomUUID().toString();
    }

    public String getLicense() {
        return license;
    }

}
