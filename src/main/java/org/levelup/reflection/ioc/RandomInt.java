package org.levelup.reflection.ioc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//@Target({ElementType.FIELD, ElementType.PARAMETER, ElementType.TYPE})
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RandomInt {

    // параметр аннотации
    int min();

    int max() default 10;

}
