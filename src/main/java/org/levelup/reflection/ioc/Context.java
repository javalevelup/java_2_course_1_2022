package org.levelup.reflection.ioc;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Random;

// 1. Make Context generic
// 2. Pass argument to getObject - Class<>, cast in return
// 3. Make getObject generic
public class Context {

    // Bank bank = (Bank) Context.getObject()
    // Account account = (Account) Context.getObject();
    // Client client = (Client) Context.getObject();

    // <T> Object getObject(List<T> list, T object)
    // <T> T getObject(List<T> list)
    // <T> T getObject() {  T t = ...; return (T) obj; };

    // Bank b = getObject();

    // Context.getObject(Bank.class) -> Bank b = ...
    // Context.class.getName() -> org.levelup.reflection.ioc.Context
    // Context.class.getSimpleName() -> Context
    public static <T> T getObject(Class<T> clazz) {
        try {
            // Ищем конструктор без параметров
            Constructor<T> defaultConstructor = clazz.getDeclaredConstructor();
            defaultConstructor.setAccessible(true);
            T instance = defaultConstructor.newInstance();

            processRandomIntAnnotation(instance);

            return instance;
        } catch (NoSuchMethodException exc) {
            System.out.println("Couldn't find default constructor in class " + clazz.getName());
            // Если мы не можем найти конструктор без параметров, то завершает программу с ошибкой
            throw new RuntimeException(exc);
        } catch (Exception exc) {
            System.out.println("Couldn't create object of class " + clazz.getName() + " using default constructor");
            throw new RuntimeException(exc);
        }
    }

    private static void processRandomIntAnnotation(Object obj) throws IllegalAccessException {
        Class<?> clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        Random random = new Random();
        for (Field field : fields) {
            RandomInt annotation = field.getAnnotation(RandomInt.class);
            if (annotation != null) {
                // Над полем присутствует аннотация RandomInt

                // Проверить, что тип поля, над которым стоит аннотация является int
                checkIntType(field);
                // [min, min + max)

                // nextInt(14) -> [0, 14) [0, 13]
                // nextInt(14) + 4 -> [0, 13] + 4 -> [4, 17]
                // [13, 25]
                // nextInt(?) + 13 -> nextInt(26 - 13) -> nextInt(13) [0, 12] + 13 -> [13, 25]
                int randomValue = random.nextInt(annotation.max() - annotation.min()) + annotation.min();

                field.setAccessible(true);
                field.set(obj, randomValue); // obj.field = randomValue;
            }
        }
    }

    private static void checkIntType(Field field) {
        Class<?> fieldTypeClass = field.getType();
        if (fieldTypeClass != int.class && fieldTypeClass != Integer.class) {
            throw new RuntimeException("Field with RandomInt annotation must be int or Integer");
        }
    }

}
