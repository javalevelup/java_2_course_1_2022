package org.levelup.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionApp {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {

        // 1 var
        Bank bank = new Bank("VTB");
        Bank sber = new Bank("Sber");
        Object objectBank = bank;

        Class<?> objectBankClass = objectBank.getClass();
        Class<?> bankClass = bank.getClass();
        Class<?> sberbankClass = sber.getClass();

        System.out.println("Are classes equal: " + (objectBankClass == bankClass));
        System.out.println("Are classes equal: " + (sberbankClass == bankClass));

        // 2 var
        Class<?> literalClass = Bank.class;
        System.out.println();

        //
        Field[] fields = bankClass.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field.getModifiers() + " " + field.getName() + " " + field.getType().getSimpleName());
        }

        Constructor<?>[] constructors = bankClass.getDeclaredConstructors();
        for (Constructor<?> constructor : constructors) {
            Class<?>[] constructorTypes = constructor.getParameterTypes();
            // (Class1, Class2, Class3)
            String types = "(";
            for (Class<?> type : constructorTypes) {
                types += type.getSimpleName() + ","; // 1. (String, 2. (String, String,
            }
            // (String, String, -> (String, String
            // (String, String -> (String, String)
            types = types.substring(0, types.length() - 1) + ")";
            System.out.println("Constructor: " + types);
        }

        System.out.println();
        System.out.println("Methods: ");
        Method[] methods = bankClass.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method.getReturnType().getSimpleName() + " " + method.getName());
        }

        System.out.println();
        System.out.println();

        // Bank bank, Clas<?> bankClass
        System.out.println("Bank license (got using method): " + bank.getLicense());
        // Получение значения приватного поля через Reflection
        Field licenseField = bankClass.getDeclaredField("license");
        licenseField.setAccessible(true);
        String licenseValue = (String) licenseField.get(bank); // bank.license
        System.out.println("Bank license (got using Reflection): " + licenseValue);

        System.out.println();
        System.out.println();

        // int.class
        Constructor<?> privateConstructor = bankClass.getDeclaredConstructor(String.class, String.class);
        privateConstructor.setAccessible(true);
        Bank tinkoffBank = (Bank) privateConstructor.newInstance("Tinkoff", "123456"); // new Bank("", "")
        System.out.println("Bank created through private constructor: " + tinkoffBank.name + " " + tinkoffBank.getLicense());

        System.out.println();
        System.out.println();

        Method regenerateLicenceMethod = bankClass.getDeclaredMethod("regenerateLicense");
        regenerateLicenceMethod.setAccessible(true);
        regenerateLicenceMethod.invoke(tinkoffBank); // tinkoffBank.regenerateLicense();
        System.out.println("Tinkoff bank license: " + tinkoffBank.getLicense());

    }

}
