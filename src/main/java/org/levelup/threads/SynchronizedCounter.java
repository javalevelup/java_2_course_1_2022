package org.levelup.threads;

public class SynchronizedCounter implements Counter {

    private int counter;

    // t1, t2, t3
    //
    @Override
    public synchronized void increment() {
        counter++;
    }

    @Override
    public int getValue() {
        synchronized (this) {
            return counter;
        }
    }

}
