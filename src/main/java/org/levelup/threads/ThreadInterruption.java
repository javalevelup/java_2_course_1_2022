package org.levelup.threads;

public class ThreadInterruption {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Printer());
        thread.start();

        // thread.stop(); аналог kill -9

        // void t1.interrupt();
        // boolean t1.isInterrupted();
        // boolean Thread.interrupted(); - static method

        thread.interrupt();
        Thread.sleep(2000); // sleep main for 2 seconds
        thread.interrupt();
        Thread.sleep(2000);
        thread.interrupt();

        if (thread.isInterrupted()) {
            System.out.println("We stopped the thread1");
        }

        Thread thread2 = new Thread(new PrinterV2());
        thread2.start();

        Thread.sleep(3000);
        System.out.println("Send interrupt status to thread2");
        thread2.interrupt();

    }

    static class PrinterV2 implements Runnable {

        @Override
        public void run() {
            boolean isInterrupted = false;
            while (!isInterrupted) {
                try {
                    Thread.sleep(200);
                    System.out.println("Thread is working...");

                    isInterrupted = Thread.currentThread().isInterrupted();
                } catch (InterruptedException exc) {
                    // флаг прерывания всегда false!
                    isInterrupted = true;
                }
            }
        }

    }

    static class Printer implements Runnable {

        @Override
        public void run() {
            System.out.println("Thread started");
            int interruptionCount = 0;
            while (true) {
                // ... some code ...
                if (Thread.currentThread().isInterrupted()) {
                    interruptionCount++;
                    if (interruptionCount == 3) {
                        break;
                    } else {
                        System.out.println("Thread interruption count: " + interruptionCount);
                        Thread.interrupted(); // сбросим флаг прерывания (вернем в false)
                    }
                }
            }
            System.out.println("Thread finished...");
        }

    }

}
