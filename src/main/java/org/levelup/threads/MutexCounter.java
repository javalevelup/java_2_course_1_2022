package org.levelup.threads;

public class MutexCounter implements Counter {

    private final Object mutex = new Object();

    private int counter;

    @Override
    public void increment() {
        // acquire - взять блокировку
        // release - освободить блокировку
        synchronized (mutex) {
            System.out.println(getValue());
            counter++;
        }
    }

    @Override
    public int getValue() {
        synchronized (mutex) {
            return counter;
        }
    }

}
