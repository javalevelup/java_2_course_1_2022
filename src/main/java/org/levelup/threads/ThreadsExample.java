package org.levelup.threads;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

public class ThreadsExample {

    public static void main(String[] args) throws InterruptedException {
        Counter c = new Counter();
        Thread counterThread1 = new Thread(new CountCallWorker(c), "count-call-1");
        Thread counterThread2 = new Thread(new CountCallWorker(c), "count-call-2");
        Thread counterThread3 = new Thread(new CountCallWorker(c), "count-call-3");

        counterThread1.start();
        counterThread2.start();
        // counterThread1.join();  // Поток, в котором вызвали метод join, ждет окончания работы потока, у которого вызвали метод join
                                   // Поток main ожидает завершения потока counterThread1
        counterThread3.start();

        ////////////////////////////////////
        Thread thread = new MyFirstThread();
        // thread.run();
        thread.start(); // поток main запустил создание и запуск потока Thread-0

        System.out.println(Thread.currentThread().getName() + ": hello from main thread");

        counterThread1.join();
        counterThread2.join();
        counterThread3.join();

        System.out.println("Counter result: " + c.getCounter());

    } // Поток main завершился

    static class MyFirstThread extends Thread {

        @Override
        public void run() { // Здесь все, что выполнится в отдельном потоке
            System.out.println(getName() + ": hello from another thread");
        } // Поток завершился

    }

    static class Counter {
        private int counter;
        public void increment() {
            counter++;
        }
        public int getCounter() {
            return counter;
        }
    }

    @RequiredArgsConstructor
    static class CountCallWorker implements Runnable {

        private final Counter c;

        @Override
        @SneakyThrows
        public void run() {
            for (int i = 0; i < 40; i++) {
                System.out.println(Thread.currentThread().getName() + " " + i);
                Thread.sleep(100); // 100 ms (1000ms - 1s)
                c.increment();
            }
        }
    }

}
