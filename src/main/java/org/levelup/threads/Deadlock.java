package org.levelup.threads;

public class Deadlock {

    public static void transferMoney(Integer accountId1, Integer accountId2, int amount) {
        Integer first = accountId1 < accountId2 ? accountId1 : accountId2;
        Integer second = first == accountId1 ? accountId2 : accountId1;

        synchronized (first) {
            synchronized (second) {
                //....
                System.out.println(amount);
            }
        }
    }

    public static void main(String[] args) {

        Integer accountId1 = 2343;
        Integer accountId2 = 2345;

        Integer accountId3 = 123;
        Integer accountId4 = 125;

        Integer accountId5 = 2134;

        Thread t1 = new Thread(() -> {
            transferMoney(accountId1, accountId2, 4);
        });

        Thread t3 = new Thread(() -> {
            transferMoney(accountId3, accountId4, 5);
        });

        Thread t4 = new Thread(() -> {
            transferMoney(accountId5, accountId1, 6);
        });

        Thread t2 = new Thread(() -> {
            transferMoney(accountId2, accountId1, 5);
        });

        t1.start();
        t2.start();


    }

}
