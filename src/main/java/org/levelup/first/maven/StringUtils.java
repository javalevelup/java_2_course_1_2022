package org.levelup.first.maven;

public class StringUtils {

    // blank string - null, empty string "", string that consists only from whitespaces "    "
    public static boolean isBlank(String value) {
        // trim() - delete all whitespaces from the begin of string and from end of string
        // "   asdf   ".trim() -> "asdf"
        // "  asd  asdd   ".trim() -> "asd asdd"
        return value == null || value.trim().isEmpty();
    }

}
