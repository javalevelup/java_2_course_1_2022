package org.levelup.first.maven.comparison;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Client {

    private Long clientId;
    private List<Account> accounts;

}
