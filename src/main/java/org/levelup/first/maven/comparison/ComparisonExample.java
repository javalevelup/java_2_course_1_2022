package org.levelup.first.maven.comparison;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Predicate;

@SuppressWarnings("ALL")
public class ComparisonExample {

    public static void main(String[] args) {

        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account(450L, "1230490954", LocalDateTime.now()));
        accounts.add(new Account(423L, "5492344332", LocalDateTime.now()));
        accounts.add(new Account(112L, "2350934934", LocalDateTime.now()));
        accounts.add(new Account(153L, "3142524324", LocalDateTime.now()));
        accounts.add(new Account(243L, "1234568654", LocalDateTime.now()));

        // $1
        Comparator<Account> accountNumberDescComparator = new Comparator<Account>() {
            @Override
            public int compare(Account o1, Account o2) {
                return -o1.getAccountNumber().compareTo(o2.getAccountNumber());
            }
        };

        // $2
//        Comparator<Account> accountNumberDescComparator2 = new Comparator<Account>() {
//            @Override
//            public int compare(Account o1, Account o2) {
//                return -o1.getAccountNumber().compareTo(o2.getAccountNumber());
//            }
//        };


        accounts.sort(accountNumberDescComparator);
        System.out.println(accounts);

        accounts.sort(new Comparator<Account>() {
            @Override
            public int compare(Account o1, Account o2) {
                return -Long.compare(o1.getAccountId(), o2.getAccountId());
            }
        });
        System.out.println(accounts);

        // ... code
        int maxLenth = 4; // must be final or effective final
        Predicate<Account> predicate = new StringLengthPredicate(maxLenth);
        // ... code
        Predicate<Account> accountLengthPredicate = new Predicate<Account>() {
            @Override
            public boolean test(Account account) {
                return account.getAccountNumber().length() < maxLenth;
            }
        };

        // (name of args) -> { // method's body }
        Predicate<Account> lambdaPredicate = (acc) -> { return acc.getAccountNumber().length() < maxLenth; };
        Predicate<Account> lambda = account -> account.getAccountNumber().length() < maxLenth;

        Comparator<Account> lambdaNumberDescComparator = (acc1, acc2) -> -acc1.getAccountNumber().compareTo(acc2.getAccountNumber());
        accounts.sort(lambdaNumberDescComparator);

        accounts.sort((acc1, acc2) -> -Long.compare(acc1.getAccountId(), acc2.getAccountId()));

//        Account account = new Account(34L, "2341325123", LocalDateTime.now()) {
//            @Override
//            public Long getAccountId() {
//                return 0L;
//            }
//        };

    }

}
