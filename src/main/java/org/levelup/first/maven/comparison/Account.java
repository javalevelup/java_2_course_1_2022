package org.levelup.first.maven.comparison;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Account implements Comparable<Account> {

    private Long accountId;
    private String accountNumber;
    private LocalDateTime openDate;

    @Override
    public int compareTo(Account o) {
        // this - first object
        // o - second object

        // < 0 (-1) - first < second
        // = 0 (0)  - first = second
        // > 0 (1)  - first > second

        return Long.compare(accountId, o.accountId);
    }

}
