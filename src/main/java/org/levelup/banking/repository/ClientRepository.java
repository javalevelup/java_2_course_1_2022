package org.levelup.banking.repository;

import org.levelup.banking.domain.Client;

import java.util.Collection;

/**
 * Интерфейс для доступа к данным таблицы clients
 * DAO - Data Access Objects
 */
public interface ClientRepository {

    Client create(String name, String lastName, String passportSeries, String passportNumber);

    /**
     * Назначает менеджера конкретному клиенту
     *
     * @param clientId ID клинта
     * @param managerId ID менеджера
     * @return объект клиента
     */
    Client assignManager(Long clientId, Long managerId);

    Collection<Client> findAll();

}
