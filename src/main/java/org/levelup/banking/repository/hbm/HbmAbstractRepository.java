package org.levelup.banking.repository.hbm;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.function.Function;

@RequiredArgsConstructor
public abstract class HbmAbstractRepository {

    protected final SessionFactory factory;

    // return runInTransaction(... -> {
    //            Client client = session.get(Client.class, clientId);
    //            Manager manager = session.load(Manager.class, managerId);
    //            client.setManager(manager);
    //            return client;
    // });
    protected <R> R runInTransaction(Function<Session, R> function) {
        try (Session session = factory.openSession()) {
            // ACID
            // A - atomicity
            // C - consistency
            // I - isolation
            // D - durability
            Transaction tx = session.beginTransaction();

            // ... execute code
            R result = function.apply(session);
            tx.commit();

            return result;
        }
    }

}
