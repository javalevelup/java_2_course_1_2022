package org.levelup.banking.repository;

import org.levelup.banking.domain.Manager;

import java.util.List;

public interface ManagerRepository {

    Manager create(String name, String lastName, String contactNumber);

    List<Manager> findAll();

    Manager findByContactNumber(String contactNumber);

}
