package org.levelup.banking;

import org.levelup.banking.domain.Client;
import org.levelup.banking.jdbc.pool.ConnectionServiceFactory;
import org.levelup.banking.repository.ClientRepository;
import org.levelup.banking.jdbc.JdbcClientRepository;
import org.levelup.banking.jdbc.pool.JdbcConnectionService;

import java.util.Collection;

public class Application {

    public static void main(String[] args) {
        ClientRepository clientRepository = new JdbcClientRepository(ConnectionServiceFactory.getConnectionService());

        Client createdClient = clientRepository.create(
                "Dmitry", "Original", "4021", "122334"
        );
        System.out.println(createdClient.getClientId() + " " +
                createdClient.getName() + " " +
                createdClient.getLastName() + " " +
                createdClient.getPassportSeries() + " " +
                createdClient.getPassportNumber()
        );
        System.out.println();

        Collection<Client> clients = clientRepository.findAll();
        for (Client client : clients) {
            System.out.println(client.getClientId() + " " +
                    client.getName() + " " +
                    client.getLastName() + " " +
                    client.getPassportSeries() + " " +
                    client.getPassportNumber()
            );
        }
    }

}
