package org.levelup.banking.jdbc.pool;

import java.sql.Connection;

public interface ConnectionService {

    Connection openNewConnection();

}
