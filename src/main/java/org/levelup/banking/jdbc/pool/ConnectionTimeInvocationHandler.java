package org.levelup.banking.jdbc.pool;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ConnectionTimeInvocationHandler implements InvocationHandler {

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        ConnectionTime annotation = method.getAnnotation(ConnectionTime.class);
        if (annotation != null) {
            long startTime = System.nanoTime();
            Object result = method.invoke(args);
            long endTime = System.nanoTime();

            System.out.println("Time of getting connection from the pool: " + (endTime - startTime) + "ns");
            return result;
        }
        return method.invoke(args);
    }

}
