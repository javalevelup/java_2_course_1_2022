package org.levelup.banking.jdbc.pool;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Класс, который управляет соединениями к БД через JDBC
 */
public class JdbcConnectionService implements ConnectionService {

    private ConnectionPool connectionPool;

    public JdbcConnectionService() {
        this.connectionPool = new ConnectionPool();
        for (int i = 0; i < 5; i++) {
            this.connectionPool.createConnectionInPool(createNewConnection());
        }
    }

    @ConnectionTime
    public Connection openNewConnection() {
        // Proxy
        Connection connection = connectionPool.getConnection();
        Class<?> connectionClass = connection.getClass();

        Connection proxyConnection = (Connection) Proxy.newProxyInstance(
                connectionClass.getClassLoader(),
                connectionClass.getInterfaces(),
                new ConnectionInvocationHandler(connection, connectionPool)
        );

        return proxyConnection;
    }

    /**
     * Открывает новое соединение к БД
     * @return объект Connection
     */
    private Connection createNewConnection() {
        try {
            // открывает новое соединение к БД
            return DriverManager.getConnection(
                    // serverTimezone=UTC&useSsl=false
                    // jdbc:<vendor_name>://<host>:<port>/<database_name>?<param1_key>=<param1_value>&<param2_key>=<param2_value>
                    "jdbc:postgresql://127.0.0.1:5432/banks",
                    "postgres",
                    "root"
            );
        } catch (SQLException exc) {
            throw new RuntimeException(exc);
        }
    }

}
