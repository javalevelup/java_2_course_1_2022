package org.levelup.banking.jdbc.pool;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;

public class ConnectionInvocationHandler implements InvocationHandler {

    private Connection connection;
    private ConnectionPool connectionPool;

    public ConnectionInvocationHandler(Connection connection, ConnectionPool connectionPool) {
        this.connection = connection;
        this.connectionPool = connectionPool;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();

        if (methodName.equals("close")) {
            System.out.println("Close method has been invoked. Return connection to pool");
            connectionPool.returnConnection(connection);
            return null;
        }

        // Вызываем метод у оригинального объекта connection
        return method.invoke(connection, args);
    }

}
