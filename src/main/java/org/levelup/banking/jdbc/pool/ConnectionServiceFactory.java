package org.levelup.banking.jdbc.pool;

import java.lang.reflect.Proxy;

public class ConnectionServiceFactory {

    private ConnectionServiceFactory() {}

    public static ConnectionService getConnectionService() {
        JdbcConnectionService jdbcConnectionService = new JdbcConnectionService();
        return (ConnectionService) Proxy.newProxyInstance(
                jdbcConnectionService.getClass().getClassLoader(),
                jdbcConnectionService.getClass().getInterfaces(),
                new ConnectionTimeInvocationHandler()
        );
    }

}
