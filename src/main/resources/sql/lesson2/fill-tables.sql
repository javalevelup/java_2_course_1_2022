insert into managers (name, last_name, contact_number)
values
    ('Maksim', 'Kurenkov', '89235754545'),  -- (nextval('managers_manager_id_seq'), 'Maksim', 'Kurenkov', '89235754545')
    ('Elena', 'Ivanova', '89214560967'),
    ('Anna', 'Simonova', '89045785634');

update clients set manager_id = 2 where client_id in (1, 4);
update clients set manager_id = 3 where client_id = 5;
update clients set manager_id = 4 where client_id in (2, 3, 6);

insert into client_info
values
    (3, '89214236745', '1992-05-19', null, 1),
    (5, '89219549754', '1993-09-12', null, 0);

insert into accounts (account_number, open_date)
values
    ('785738234773623', '2021-02-12 12:42:45'),
    ('794827347234757', '2022-01-24 17:43:11'),
    ('764387691237478', '2017-10-06 14:00:23');

insert into clients_accounts
values
    (3, 3),
    (5, 3),
    (1, 1),
    (6, 2);